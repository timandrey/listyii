<?php


namespace backend\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\form\AddRoleForm;

class RoleController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['accept'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['index', 'add-role'],
                    'allow' => true,
                    'roles' => ['admin'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex() {
        $add_role_form = new AddRoleForm();
        return $this->render('addRole', compact('add_role_form'));
    }

    public function actionAddRole() {
        $add_role_form = new AddRoleForm();
        if ($add_role_form->load(Yii::$app->request->post()) && $add_role_form->validate()) {
            $role = Yii::$app->authManager->createRole($add_role_form->name);
            $role->description = $add_role_form->description;
            Yii::$app->authManager->add($role);
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

}
<?php


namespace backend\models\form;

use yii\base\Model;

class UserEditForm extends Model
{
    public $username;
    public $email;
    public $user_id;

    public function rules() {
        return [
            [['username', 'email'], 'string', 'max' => 33],
            ['email', 'email'],
            ['user_id', 'number'],
        ];
    }
}
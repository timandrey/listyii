<?php
namespace backend\models\form;

use Yii;

/**
 * Login form
 */
class LoginForm extends \common\models\form\LoginForm
{
    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();

            $roles = Yii::$app->authManager->getRolesByUser($user->id);
            $role = array_shift($roles)->name;
            if ($role != 'admin' && $role != 'manager')  {
                return false;
            }
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        
        return false;
    }
}

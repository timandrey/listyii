<?php


namespace api\controllers;

use common\models\Checklist;
use common\models\Listitem;
use common\models\form\CreateListitem;
use Yii;

class ListitemController extends BaseController
{
    public $modelClass = 'api\models\Listitem';

    public function actionIndex($list_id) {
        $checklist = Checklist::findOne($list_id);
        if (Yii::$app->user->can('updateOwnNews', ['checklist' => $checklist])) {
            return Yii::$app->api->sendSuccessResponse([
                'checklist_title'  => $checklist->title,
                'checklist_items' => $checklist->listitems]);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['dont show']);
        }

    }

    public function actionView($list_id, $item_id) {
        $checklist = Checklist::findOne($list_id);
        $listitem = Listitem::findOne($item_id);
        if (Yii::$app->user->can('updateOwnNews', ['checklist' => $checklist])) {
            return Yii::$app->api->sendSuccessResponse([$listitem]);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['dont show']);
        }
    }

    public function actionCreate($list_id) {
        $create_listitem_model = new CreateListitem();
        if ($create_listitem_model->load(Yii::$app->getRequest()->getBodyParams(), '') && $create_listitem_model->create($list_id)) {
            return Yii::$app->api->sendSuccessResponse(['create listitem: true']);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['create listitem: false']);
        }

    }

    public function actionUpdate($list_id, $item_id) {
        $checklist = Checklist::findOne($list_id);
        $listitem =Listitem::findOne($item_id);

        if (Yii::$app->user->can('updateOwnNews', ['checklist' => $checklist])) {
            $body = Yii::$app->request->post('body');
            $done = Yii::$app->request->post('done');

            if ($body != null) {
                $listitem->body = $body;
            }
            if ($done != null) {
                $listitem->done = $done;
            }

            $listitem->save();
            return Yii::$app->api->sendSuccessResponse([$listitem]);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['dont update']);
        }
    }

    public function actionDelete($list_id, $item_id) {
        $checklist = Checklist::findOne($list_id);
        $listitem = Listitem::findOne($item_id);
        if (Yii::$app->user->can('updateOwnNews', ['checklist' => $checklist])) {
            $listitem->delete();
            return Yii::$app->api->sendSuccessResponse(['delete is true']);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['dont delete']);
        }
    }
}
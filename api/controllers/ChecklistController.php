<?php


namespace api\controllers;

use common\models\form\CreateChecklist;
use common\models\User;
use Yii;
use common\models\Checklist;

class ChecklistController extends BaseController
{
    public $modelClass = 'api\models\Checklist';

    public function actionIndex() {
        $checklists = User::findOne(Yii::$app->user->id)->checklists;
        return Yii::$app->api->sendSuccessResponse([$checklists]);
    }

    public function actionView($id) {
        $checklist = Checklist::findOne($id);
        if (Yii::$app->user->can('updateOwnNews', ['checklist' => $checklist])) {
            return Yii::$app->api->sendSuccessResponse([$checklist]);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['dont show']);
        }
    }

    public function actionCreate() {
        $create_list_model = new CreateChecklist();

        if ($create_list_model->load(Yii::$app->getRequest()->getBodyParams(), '') && $create_list_model->create()) {
            return Yii::$app->api->sendSuccessResponse(['create true']);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['create false']);
        }
    }

    public function actionDelete($id) {
        $checklist = Checklist::findOne($id);

        if (Yii::$app->user->can('updateOwnNews', ['checklist' => $checklist])) {
            $checklist->delete();
            return Yii::$app->api->sendSuccessResponse(['delete true']);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['dont delete']);
        }
    }

    public function actionUpdate($id) {
        $checklist = Checklist::findOne($id);

        if (Yii::$app->user->can('updateOwnNews', ['checklist' => $checklist])) {
            $title = Yii::$app->request->post('title');
            $checklist->title = $title;
            $checklist->save();
            return Yii::$app->api->sendSuccessResponse([$checklist]);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['dont update']);
        }
    }

}
<?php
namespace api\models\auth;

use Yii;
/**
 * Login form
 */
class LoginForm extends \common\models\form\LoginForm
{
    Const EXPIRE_TIME = 604800; //7 days valid

    public function login()
    {
        if ($this->validate()) {
            if ($this->getUser()) {
                $access_token =  $this->_user->generateAccessToken();
                $this->_user->expire_at = time() + static::EXPIRE_TIME;
                $this->_user->save();
                Yii::$app->user->login($this->_user, static::EXPIRE_TIME);
                return $access_token;
            }
        }
        return false;
    }
}

<?php

use yii\db\Migration;

/**
 * Class m191022_103223_add_max_cecklists_user_table
 */
class m191022_103223_add_max_cecklists_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'max_lists', $this->integer()->defaultValue(15));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191022_103223_add_max_cecklists_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191022_103223_add_max_cecklists_user_table cannot be reverted.\n";

        return false;
    }
    */
}

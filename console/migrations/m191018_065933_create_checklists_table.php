<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%checklists}}`.
 */
class m191018_065933_create_checklists_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%checklist}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'title' => $this->string(),
            'created_at' => $this->timestamp(),
        ]);

        $this->addForeignKey(
            'userId',  // это "условное имя" ключа
            '{{%checklist}}', // это название текущей таблицы
            'user_id', // это имя поля в текущей таблице, которое будет ключом
            '{{%user}}', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE', // что делать при удалении сущности, на которую ссылаемся
            'CASCADE'  // что делать при обновлении сущности, на которую ссылаемся
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%checklists}}');
        $this->dropForeignKey(
            'user_id',
            'users'
        );
    }
}

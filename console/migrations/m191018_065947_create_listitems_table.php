<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%listitems}}`.
 */
class m191018_065947_create_listitems_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%listitem}}', [
            'id' => $this->primaryKey(),
            'checklist_id' => $this->integer(),
            'body' => $this->string(),
            'done' => $this->boolean()->defaultValue(false),
            'created_at' => $this->timestamp(),
        ]);

        $this->addForeignKey(
            'checklistId',  // это "условное имя" ключа
            '{{%listitem}}', // это название текущей таблицы
            'checklist_id', // это имя поля в текущей таблице, которое будет ключом
            '{{%checklist}}', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE', // что делать при удалении сущности, на которую ссылаемся
            'CASCADE'  // что делать при обновлении сущности, на которую ссылаемся
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%listitems}}');
    }
}

<?php

use yii\db\Migration;
use backend\models\User;
/**
 * Class m191018_082047_create_admin_user
 */
class m191018_082047_create_admin_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $transaction = $this->getDb()->beginTransaction();
        $user = \Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'create',
            'email'    => 'admin',
            'username' => 'admin2@example.com',
            'password_hash' => Yii::$app->security->generatePasswordHash('secret'),
        ]);
        if (!$user->insert(false)) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191018_082047_create_admin_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191018_082047_create_admin_user cannot be reverted.\n";

        return false;
    }
    */
}

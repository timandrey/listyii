<?php


namespace common\models;

use yii\db\ActiveRecord;

class Listitem extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%listitem}}';
    }
}
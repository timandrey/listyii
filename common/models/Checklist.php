<?php


namespace common\models;

use yii\db\ActiveRecord;

class Checklist extends ActiveRecord
{
    public function getUser(){
        return $this->hasOne(User::className(), ['user_id' => 'id']);
    }

    public function getListitems()
    {
        return $this->hasMany(Listitem::className(), ['checklist_id' => 'id']);
    }
}
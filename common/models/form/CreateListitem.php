<?php


namespace common\models\form;

use Yii;
use yii\base\Model;
use common\models\Listitem;

class CreateListitem extends Model
{
    public $body;

    public function rules()
    {
        return [
            ['body', 'required'],
        ];
    }

    public function create($list_id) {
        if (!$this->validate()) {
            return null;
        }

        $listitem = new Listitem();
        $listitem->body = $this->body;
        $listitem->checklist_id = $list_id;

        return $listitem->save();
    }
}
<?php


namespace common\models\form;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\Checklist;

class CreateChecklist extends Model
{
    public $title;

    public function rules()
    {
        return [
            ['title', 'required']
        ];
    }

    public function create() {

        if (!$this->validate()) {
            return null;
        }

        $checklist = new Checklist();
        $checklist->title = $this->title;
        $checklist->user_id = Yii::$app->user->id;
        $user_count_lists = Checklist::find()->where(['user_id' => Yii::$app->user->id])->count();

        if ($user_count_lists + 1 > User::findOne(Yii::$app->user->id)->getMaxLists()) {
            return false;
        }
        return $checklist->save();
    }
}
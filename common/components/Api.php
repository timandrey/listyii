<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\Json;
use yii\web\Response;
/**
 * Class for common API functions
 */
class Api extends Component
{
    public function sendFailedResponse($message)
    {
        $this->setHeader(400);
        $response = array('status' => 0, 'error_code' => 400, 'errors' => $message);
        return $response;
        //return ($response);
    }
    public function sendSuccessResponse($data = false)
    {
        $this->setHeader(200);
        $response = [];
        $response['status'] = 1;
        if (is_array($data))
            $response['data'] = $data;

        return $response;
    }
    protected function setHeader($status)
    {
        $text = $this->_getStatusCodeMessage($status);
        Yii::$app->response->setStatusCode($status, $text);
        Yii::$app->response->format = Response::FORMAT_JSON;
    }
    protected function _getStatusCodeMessage($status)
    {
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
}